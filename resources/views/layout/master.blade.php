<!DOCTYPE html>
<html lang="">
    <head>
        <title>Book tour management</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('layout_frontend/frontend.css') }}">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    </head>
    <body>
        <header class="container">
        <div class="row">
            <div class="col-sm-2 text-left">
                <div class="text-center">
                    <img class="logo" src="{{ asset('images/logo.png') }}" alt="logo" width="100" height="100">
                </div>
                <div id="trademark" class="text-center">THANH TRAVEL</div>
            </div>
            <div class="col-sm-10 text-right">
                <div class="margin-top-20px">
                    <a href="#">Giới thiệu về Thanh Travel </a>|
                    <a href="#">Liên hệ </a>
                </div>
                <div class="margin-top-20px">
                    <a href="#">
                        <img class="logo" src="{{ asset('images/icons/facebook.png') }}" alt="l" width="40" height="40">
                    </a>
                    <a href="#">
                        <img class="logo" src="{{ asset('images/icons/youtube.png') }}" alt="l" width="40" height="40">
                    </a>
                    <a href="#">
                        <img class="logo" src="{{ asset('images/icons/google.png') }}" alt="l" width="45" height="45">
                    </a>
                </div>
            </div>
        </div>
    </header>
        <nav class="navbar container">
            <div class="container list-menu">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Trang chủ</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Du lịch nước ngoài</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Du lịch trong nước</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Lịch khởi hành</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Khuyến mãi</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Tin tức</a>
                </div>
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Liên hệ</a>
                </div>
            </div>
        </nav>
        <div id="filter-area" class="container text-left">
            <form class="form-inline filter-area">
                <div class="form-check form-check-inline pb-15">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1">Trong nước</label>
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                    <label class="form-check-label" for="inlineRadio2">Nước ngoài</label>
                </div>
                <div class="form-group">
                    <label for="inputPassword2">Điểm đến</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option selected>--Điểm đến--</option>
                        <option>Hà nội</option>
                        <option>Huế</option>
                        <option>TP HCM</option>
                        <option>Đà Nẵng</option>
                        <option>Hội An</option>
                    </select>
                    <label for="inputPassword2" class="pl-30">Giá tour</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option selected>--Giá tour--</option>
                        <option>1.000.000đ - 5.000.000</option>
                        <option>5.000.000đ - 10.000.000</option>
                        <option>10.000.000đ - 15.000.000</option>
                        <option>15.000.000đ - 20.000.000</option>
                    </select>
                    <label for="inputPassword2" class="pl-30">Lịch khởi hành</label>
                    <input type="text" class="form-control" id="datepicker">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Tìm kiếm</button>
            </form>
        </div>
        <div class="container text-center mt-15">
            <div class="row content">
                <div class="col-sm-9 text-left">
                    @yield('content')
                </div>
                <div class="col-sm-3 ">
                    <div class="well">
                        <p>Bạn chưa có tài khoản ?</p>
                        <p>Vui lòng nhấn vào <a href="">đây</a> để đăng ký </p>
                        <button class="btn btn-success">Đăng nhập</button>
                    </div>
                    <div class="support">
                        <h4 class="support-title">HỖ TRỢ TRỰC TUYẾN</h4>
                        <div class="support-item">
                            <p class="support-position">Kinh doanh</p>
                            <div class="support-info">
                                <p>Name</p>
                                <p>09099089222</p>
                            </div>
                        </div>
                        <div class="support-item">
                            <p class="support-position">Tư vấn</p>
                            <div class="support-info">
                                <p>Name</p>
                                <p>09099089222</p>
                            </div>
                        </div>
                    </div>
                    <div class="support">
                        <h4 class="support-title">QUẢNG CÁO</h4>
                        <div class="advertise-item">

                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <footer class="container text-center">--}}
{{--        <hr>--}}
{{--        <div class="row">--}}
{{--            <div class="col-sm-4">--}}
{{--                <h4 class="footer-title">Liên hệ</h4>--}}
{{--            </div>--}}
{{--            <div class="col-sm-4">--}}
{{--                <h4 class="footer-title">19901011</h4>--}}
{{--            </div>--}}
{{--            <div class="col-sm-4">--}}
{{--                <h4 class="footer-title">Chấp nhận thanh toán</h4>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </footer>--}}
    </body>
</html>
<script>
    $(document).ready(function() {
        $( "#datepicker" ).datepicker();
    });
</script>

