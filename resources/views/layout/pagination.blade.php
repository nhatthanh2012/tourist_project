@if ($paginator->hasPages())
    <nav class="pg_user_list">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled">
                    <span><i class="fa fa-angle-double-left"></i></span>
                </li>
            @else
                <li>
                    <a class="checkNetConnection" href="{{ $elements[0][1] }}">
                        <span><i class="fa fa-angle-double-left"></i></span>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li><span class="checkNetConnection"><span>{{ $element }}</span></span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a class="checkNetConnection">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}" class="checkNetConnection">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                @php
                    $condition = \Illuminate\Support\Facades\Request::toArray();
                @endphp
                <li>
                    <a class="checkNetConnection" href="{{\Illuminate\Support\Facades\Request::fullUrl()}}{{ ($condition == null ? '?' : '&') }}page={{ $paginator->lastPage() }}">
                        <span><i class="fa fa-angle-double-right"></i></span>
                    </a>
                </li>
            @else
                <li class="disabled">
                    <span><i class="fa fa-angle-double-right"></i></span>
                </li>
            @endif
        </ul>
    </nav>
@endif
