@extends('layout.master')
@section('content')
    <div class="block">
        <div class="block-title">
            <p>Tour sắp khởi hành</p>
        </div>
        <table class="table table-hover">
            <thead>
            <tr class="row">
                <th class="col-sm-5">Tên tour</th>
                <th class="col-sm-3">Thời gian</th>
                <th class="col-sm-3">Khởi hành</th>
                <th class="col-sm-1"></th>
            </tr>
            </thead>
            <tbody>
            <tr class="row">
                <td class="col-sm-5">Tour Hội An 2N2D</td>
                <td class="col-sm-3">2 ngày 2 đêm</td>
                <td class="col-sm-3">1/12/2021</td>
                <td class="col-sm-1"><button type="button" class="btn btn-primary">Đặt tour</button></td>
            </tr>
            <tr class="row">
                <td class="col-sm-5">Tour Hội An 2N2D</td>
                <td class="col-sm-3">2 ngày 2 đêm</td>
                <td class="col-sm-3">1/12/2021</td>
                <td class="col-sm-1"><button type="button" class="btn btn-primary">Đặt tour</button></td>
            </tr>
            <tr class="row">
                <td class="col-sm-5">Tour Hội An 2N2D</td>
                <td class="col-sm-3">2 ngày 2 đêm</td>
                <td class="col-sm-3">1/12/2021</td>
                <td class="col-sm-1"><button type="button" class="btn btn-primary">Đặt tour</button></td>
            </tr>
            <tr class="row">
                <td class="col-sm-5">Tour Hội An 2N2D</td>
                <td class="col-sm-3">2 ngày 2 đêm</td>
                <td class="col-sm-3">1/12/2021</td>
                <td class="col-sm-1"><button type="button" class="btn btn-primary">Đặt tour</button></td>
            </tr>
            <tr class="row">
                <td class="col-sm-5">Tour Hội An 2N2D</td>
                <td class="col-sm-3">2 ngày 2 đêm</td>
                <td class="col-sm-3">1/12/2021</td>
                <td class="col-sm-1"><button type="button" class="btn btn-primary">Đặt tour</button></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="block">
        <div class="block-title">
            <p>Tour mới nhất</p>
        </div>
        <div class="container">
            <div class="list-tour row">
                <div class="col-sm-4 tour-item">
                    <div class="card">
                        <img class="card-img-top" width="420" src="https://www.kenvintravel.com.vn/upload/img/products/05092019/du-lich-phu-quoc-3-ngay-2-dem.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="tour-info">
                                <div>
                                    <p class="card-text">Thời gian: 3 ngày 3 đêm</p>
                                    <p class="card-text">Khởi hành: 1/12/2020</p>
                                    <p class="card-text">Số chỗ còn lại: 30</p>
                                    <p class="card-text">Giá tour: 3.100.000 VND</p>
                                </div>
                                <div class="tour-title-button">
                                    <h5 class="tour-title">Card title1</h5>
                                    <a href="#" class="btn btn-primary mb-15">ĐẶT TOUR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 tour-item">
                    <div class="card">
                        <img class="card-img-top" width="420" src="https://www.kenvintravel.com.vn/upload/img/products/05092019/du-lich-phu-quoc-3-ngay-2-dem.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="tour-info">
                                <div>
                                    <p class="card-text">Thời gian: 3 ngày 3 đêm</p>
                                    <p class="card-text">Khởi hành: 1/12/2020</p>
                                    <p class="card-text">Số chỗ còn lại: 30</p>
                                    <p class="card-text">Giá tour: 3.100.000 VND</p>
                                </div>
                                <div class="tour-title-button">
                                    <h5 class="tour-title">Card title1</h5>
                                    <a href="#" class="btn btn-primary mb-15">ĐẶT TOUR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 tour-item">
                    <div class="card">
                        <img class="card-img-top" width="420" src="https://www.kenvintravel.com.vn/upload/img/products/05092019/du-lich-phu-quoc-3-ngay-2-dem.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="tour-info">
                                <div>
                                    <p class="card-text">Thời gian: 3 ngày 3 đêm</p>
                                    <p class="card-text">Khởi hành: 1/12/2020</p>
                                    <p class="card-text">Số chỗ còn lại: 30</p>
                                    <p class="card-text">Giá tour: 3.100.000 VND</p>
                                </div>
                                <div class="tour-title-button">
                                    <h5 class="tour-title">Card title1</h5>
                                    <a href="#" class="btn btn-primary mb-15">ĐẶT TOUR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 tour-item">
                    <div class="card">
                        <img class="card-img-top" width="420" src="https://www.kenvintravel.com.vn/upload/img/products/05092019/du-lich-phu-quoc-3-ngay-2-dem.jpg" alt="Card image cap">
                        <div class="card-body">
                            <div class="tour-info">
                                <div>
                                    <p class="card-text">Thời gian: 3 ngày 3 đêm</p>
                                    <p class="card-text">Khởi hành: 1/12/2020</p>
                                    <p class="card-text">Số chỗ còn lại: 30</p>
                                    <p class="card-text">Giá tour: 3.100.000 VND</p>
                                </div>
                                <div class="tour-title-button">
                                    <h5 class="tour-title">Card title1</h5>
                                    <a href="#" class="btn btn-primary mb-15">ĐẶT TOUR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="block">
        <div class="block-title">
            <p>Bài  mới nhất</p>
        </div>
        <div class="row mt-10">
            <div class="col-sm-3">
                <img width="200" src="https://znews-photo.zadn.vn/w1024/Uploaded/asfzyreslz2/2020_11_13/2_1.jpg" alt="">
            </div>
            <div class="col-sm-9">
                <h5>Du lịch giãn cách ở Maldives - từ nhà tổ chim đến lều bong bóng</h5>
                <p>Ngày đăng: 10-10-2020</p>
                <p>Từ ăn uống trên cây đến biệt thự dưới nước, Maldives có nhiều cách độc đáo để giúp du khách đảm bảo giãn cách xã hội trong khi vẫn tận hưởng kỳ nghỉ.</p>
            </div>
        </div>
    </div>
@endsection
